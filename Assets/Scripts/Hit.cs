﻿using UnityEngine;
using System.Collections;

public class Hit : MonoBehaviour {

	public AudioClip crack;
	public Sprite[] hitSprites;
	public static int breakableCount = 0;
	public GameObject smoke;

	private int maxHits; 
	private int timesHit;
	private LevelMenager levelMenager;
	private bool isBreakable;

	
	// Use this for initialization
	void Start () {
		isBreakable = (this.tag == "Breakable");
		if (isBreakable) {
			breakableCount++;


		}
		timesHit = 0;
		levelMenager = GameObject.FindObjectOfType<LevelMenager> ();

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnCollisionEnter2D (Collision2D col) {
		AudioSource.PlayClipAtPoint (crack, transform.position);
		bool isBreakable = (this.tag == "Breakable");
		if (isBreakable) {
			HandleHits ();
		}
	}
	void HandleHits(){

		timesHit++;
		maxHits = hitSprites.Length + 1; 
	
		if (timesHit >= maxHits) {
			breakableCount --;
		
			levelMenager.HitDestoyed();
			GameObject smokePuff =Instantiate (smoke, transform.position, Quaternion.identity)as GameObject;
			smokePuff.particleSystem.startColor = gameObject.GetComponent<SpriteRenderer>().color;
			Destroy (gameObject);
		} else {
			LoadSprites ();
		}
	}

	
		void LoadSprites(){
		int spriteIndex = timesHit - 1;

		if (hitSprites [spriteIndex] != null) {
			this.GetComponent<SpriteRenderer> ().sprite = hitSprites [spriteIndex];
		} else {Debug.LogError("Brick sprite missing");
		}
	}
	
	
	// TODO Remove this method once we can actually win!
	void SimulateWin () {
		levelMenager.LoadNextLevel();
	}
}